package tests;

import haxe.Int64;
import bytetype.ByteType;
import utest.Assert;
import utest.ITest;
import models.*;


class TestByteTypeBuilder implements ITest
{
    public function new() { }


    function testCodes()
    {
        // Verify that codes match the getCode result.
        Assert.equals(TestTypeInts.code, ByteType.getCode(new TestTypeInts(1, 2, 3)));
        Assert.equals(TestTypeMixed.code, ByteType.getCode(new TestTypeMixed(1.5, 2, 3, "asdf", 4)));
        Assert.equals(TestTypeStrings.code, ByteType.getCode(new TestTypeStrings("one", "two", "three")));
        Assert.equals(TestTypeStringWrongLength.code, ByteType.getCode(new TestTypeStringWrongLength("asdf")));

        // Verify that there are no duplicates.
        var codes: Array<Int> = [ TestTypeInts.code, TestTypeMixed.code, TestTypeStrings.code, TestTypeStringWrongLength.code ];
        for (i in 0...codes.length-1)
        {
            for (j in i+1...codes.length)
            {
                Assert.notEquals(codes[i], codes[j]);
            }
        }
    }


    function testInts()
    {
        var tt: TestTypeInts = new TestTypeInts(1, 2, 3);

        Assert.equals(1, tt.one);
        Assert.equals(2, tt.two);
        Assert.equals(3, tt.three);
        Assert.equals(16, TestTypeInts.size);
    }


    function testMixed()
    {
        var tt: TestTypeMixed = new TestTypeMixed(1.5, 2, 3, "asdf", 4);

        Assert.floatEquals(1.5, tt.float);
        Assert.equals(2, tt.int);
        Assert.equals(3, tt.int64.low);
        Assert.equals(0, tt.int64.high);
        Assert.equals(4, tt.uint);
        Assert.equals("asdf", tt.string);
        Assert.equals(27, TestTypeMixed.size);
    }


    function testMixedInit()
    {
        var tt: TestTypeMixed = new TestTypeMixed(5, 8, 23, "enlo", 89);

        var initRet: TestTypeMixed = tt.reset(1.5, 2, 3, "asdf", 4);

        Assert.equals(tt, initRet);

        Assert.floatEquals(1.5, tt.float);
        Assert.equals(2, tt.int);
        Assert.equals(3, tt.int64.low);
        Assert.equals(0, tt.int64.high);
        Assert.equals(4, tt.uint);
        Assert.equals("asdf", tt.string);
        Assert.equals(27, TestTypeMixed.size);
    }


    function testMixedSetters()
    {
        var tt: TestTypeMixed = new TestTypeMixed(1.5, 2, 3, "asdf", 4);

        tt.float = 5.67;
        Assert.floatEquals(5.67, tt.float);

        tt.int = 12;
        Assert.equals(12, tt.int);

        tt.string = "hello";
        Assert.equals("hello", tt.string);
    }


    function testStrings()
    {
        var tt: TestTypeStrings = new TestTypeStrings("one", "verylong", "three");

        Assert.equals("one", tt.string1);
        Assert.equals("very", tt.string2);
        Assert.equals("thre", tt.string3);
    }


    function testStringWrongLength()
    {
        var tt: TestTypeStringWrongLength = new TestTypeStringWrongLength("12345");

        // Expect truncated.
        Assert.equals("1234", tt.string4);
    }
}
