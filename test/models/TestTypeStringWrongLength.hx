package models;

import haxe.io.Bytes;


@:build(bytetype.ByteTypeBuilder.build())
abstract TestTypeStringWrongLength(Bytes) to Bytes
{
    @length(4) var string4: String;
}
