package models;

import haxe.io.Bytes;


@:build(bytetype.ByteTypeBuilder.build())
abstract TestTypeInts(Bytes) to Bytes
{
    var one: Int;
    var two: Int;
    var three: Int;
}
