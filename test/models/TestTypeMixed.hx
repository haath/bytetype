package models;

import haxe.io.Bytes;
import haxe.Int64;


@:build(bytetype.ByteTypeBuilder.build())
abstract TestTypeMixed(Bytes) to Bytes
{
    var float: Float;
    var int: Int;
    var int64: Int64;
    @length(5) var string: String;
    var uint: UInt;
}
