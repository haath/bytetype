package bytetype;

import haxe.Int32;
import haxe.macro.Context;
import haxe.io.Bytes;
import haxe.macro.Type.ClassType;
import haxe.macro.Type.ClassField;
#if (macro)
import haxe.macro.Context;
import haxe.macro.Expr;
using haxe.macro.ExprTools;
using haxe.macro.TypeTools;
using haxe.macro.Tools;
using sys.FileSystem;
import haxe.io.Path;
using StringTools;
#end


class ByteType
{
    /**
        Reads and returns the code of the byte array.

        Codes are assigned to abstract types who use the `ByteTypeBuilder`.
        This function should be used to check which type exactly was used
        to creare this byte message.

        @param data The message to check.
    **/
    public static inline function getCode(data: Bytes): Int
    {
        return data.getInt32(0);
    }


    /**
        Generates code which checks the type code of the given `data` message, and passes it
        to its own callback. To use this macro, one callback needs to be defined with a specific naming
        convention, as shown in the example below.

        ```haxe
        ByteType.process(message, "my.messages");

        function callbackHello(msg: my.messages.Hello) { }

        function callbackGoodbye(msg: my.messages.Goodbye) { }
        ```

        @param data The `Bytes` message to process.
        @param path The package path to parse for message types.
        @param recursive Whether to search the package path recursively. Default: `true`
    **/
    @IgnoreCover
    public static macro function process(data: Expr, path: Expr, recursive: Bool = true, callbacksOptional: Bool = true)
    {
        var dispatchCode: StringBuf = new StringBuf();
        dispatchCode.add('{\n');
        dispatchCode.add('@:noCompletion var _data: haxe.io.Bytes = ${data.toString()};\n');
        dispatchCode.add('@:noCompletion var _dataSize: Int = _data.length;\n');
        dispatchCode.add( processInternal(macro _data, path, recursive, callbacksOptional) );
        dispatchCode.add('}\n');
        return Context.parse(dispatchCode.toString(), Context.currentPos());
    }


    /**
        Similar to the `process()` macro, but meant to be used when the `data` buffer is expected to possibly contain multiple
        bytetype messages packed together.

        This macro will process the `data` buffer in a loop, and all messages found will have their callbacks invoked.

        @param data The `Bytes` message to process.
        @param path The package path to parse for message types.
        @param recursive Whether to search the package path recursively. Default: `true`
    **/
    @IgnoreCover
    public static macro function processAll(data: Expr, path: Expr, recursive: Bool = true, callbacksOptional: Bool = true)
    {
        var dispatchCode = new StringBuf();
        dispatchCode.add('{\n');
        dispatchCode.add('@:noCompletion var _size: Int = -1;\n');
        dispatchCode.add('@:noCompletion var _data: haxe.io.Bytes = ${data.toString()};\n');
        dispatchCode.add('@:noCompletion var _dataSize: Int = _data.length;\n');

        var innerLoopBlock: String = processInternal(macro _data, path, recursive, callbacksOptional, macro _size);
        dispatchCode.add('
            _size = -1;

            while (_data.length > 0)
            {
                $innerLoopBlock

                if (_size == -1)
                {
                    break;
                }
                else
                {
                    _data = _data.sub(_size, _data.length - _size);
                }
            }
        ');

        dispatchCode.add('}\n');
        return Context.parse(dispatchCode.toString(), Context.currentPos());
    }


    #if (macro || UNIT_TEST)
    static final primes = [
        1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997, 1009, 1013, 1019, 1021, 1031, 1033, 1039, 1049, 1051, 1061, 1063, 1069, 1087, 1091, 1093, 1097, 1103, 1109, 1117, 1123, 1129, 1151, 1153, 1163, 1171, 1181, 1187, 1193, 1201, 1213, 1217, 1223, 1229, 1231, 1237, 1249, 1259, 1277, 1279, 1283, 1289, 1291, 1297, 1301, 1303, 1307, 1319, 1321, 1327, 1361, 1367, 1373, 1381, 1399, 1409, 1423, 1427, 1429, 1433, 1439, 1447, 1451, 1453, 1459, 1471, 1481, 1483, 1487, 1489, 1493, 1499, 1511, 1523, 1531, 1543, 1549, 1553, 1559, 1567, 1571, 1579, 1583, 1597, 1601, 1607, 1609, 1613, 1619
    ];
    public static function hash(str: String): Int
    {
        var h: Int32 = 1;
        for (i in 0...str.length)
        {
            var charCode: Int = str.charCodeAt(i);
            if (charCode < 48 || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 95) || charCode > 122)
            {
                #if macro
                Context.error('Invalid character in type name: ${str.charAt(i)} ($charCode)', Context.currentPos());
                #end
                return -1;
            }

            var prime: Int32 = primes[charCode];

            var pow: Int32 = prime;
            for (_ in 1...i) pow *= pow;

            h *= pow;
        }
        return h;
    }
    #end


    #if macro
    static function processInternal(data: Expr, path: Expr, recursive: Bool, callbacksOptional: Bool, storeSize: Expr = null): String
    {
        var packages: Array<String> = getPackagesInPath(path, recursive);

        var dataExpr: String = data.toString();

        var dispatchCode: StringBuf = new StringBuf();
        dispatchCode.add('@:noCompletion var _code: Int = bytetype.ByteType.getCode(_data);\n');
        for (i in 0...packages.length)
        {
            var pkg: String = packages[i];
            var className: String = pkg.split('.')[pkg.split('.').length - 1];
            var callbackName: String = 'callback$className';

            if (i > 0)
            {
                dispatchCode.add('else ');
            }

            dispatchCode.add('if (_code == $pkg.code) {\n');

            // Matched the message code, check if to generate the callback.
            if (!callbacksOptional || contextContainsFunction(callbackName))
            {
                // Callback should be generated, check if the data needs slicing first.
                dispatchCode.add('
                    if (_dataSize > $pkg.size)  $callbackName(cast($dataExpr.sub(0, $pkg.size), $pkg));
                    else                        $callbackName(cast($dataExpr, $pkg));
                ');
            }
            // Store the type's size.
            if (storeSize != null)
            {
                dispatchCode.add('${storeSize.toString()} = $pkg.size;\n');
            }

            dispatchCode.add('}\n');
        }

        return dispatchCode.toString();
    }


    static function getPackagesInPath(path: Expr, recursive: Bool): Array<String>
    {
        var pathStr: String = switch(path.expr)
        {
            case EConst(CString(s)): s;
            case _: path.toString();
        }

        if(~/[^a-zA-Z0-9_.]/.match(pathStr))
        {
            Context.error('The first argument for ByteType.process() should be a valid package path.', Context.currentPos());
            return [];
        }

        var pack: Array<String> = pathStr.split('.');
        var relativePath: String = Path.join(pack);

        var packages: Array<String> = [];

        for (classPath in Context.getClassPath())
        {
            traverse(packages, Path.join([classPath, relativePath]), pathStr, recursive);
        }

        return packages;
    }


    static function traverse(packages: Array<String>, dir: String, path: String, recursive: Bool)
    {
        if (!dir.exists())
        {
            return;
        }

        for (file in dir.readDirectory())
        {
            var fullPath: String = Path.join([dir, file]);

            if (fullPath.isDirectory() && recursive)
            {
                traverse(packages, fullPath, '$path.$file', true);
                continue;
            }

            if (file.substr(-3) != '.hx')
            {
                continue;
            }

            var className: String = file.substr(0, file.length - 3);
            if (className == '')
            {
                continue;
            }

            packages.push('$path.$className');
        }
    }


    static function contextContainsFunction(functionName: String): Bool
    {
        var field: ClassField = Context.getLocalClass().get().findField(functionName);

        if (field == null)
        {
            field = Context.getLocalClass().get().findField(functionName, true);
        }

        if (field != null)
        {
            switch field.kind
            {
                case FMethod(f): return true;
                default:
            }
        }
        else
        {
            for (name => tvar in Context.getLocalTVars())
            {
                if (tvar.name == functionName)
                {
                    switch tvar.t
                    {
                        case TFun(args, ret): return true;
                        default:
                    }
                }
            }
        }

        return false;
    }
    #end
}
