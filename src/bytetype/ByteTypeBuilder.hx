package bytetype;

#if (macro || doc_gen)
import haxe.macro.Type.ClassType;
import haxe.macro.Context;
import haxe.macro.Expr;
using haxe.macro.ExprTools;
using haxe.macro.TypeTools;
using StringTools;


class ByteTypeBuilder
{
    static inline var dataPointerStart: Int = 4;


    public static function build(): Array<Field>
    {
        #if macro
        var fields: Array<Field> = Context.getBuildFields();

        /*
            Add datatype code.
        */
        var code: Int = ByteType.hash(Context.getLocalClass().get().name);
        fields.push({
            name: 'code',
            pos: Context.currentPos(),
            access: [ APublic, AStatic, AInline ],
            kind: FVar(macro: Int, macro $v{code}),
            doc: 'Unique code assigned to this type.'
        });

        // Map constructor args to their length.
        var ctorArgs: Map<FunctionArg, Int> = [];

        /*
            Create field getters and setters.
        */
        var dataPointer: Int = dataPointerStart;
        for (field in fields)
        {
            switch (field.kind)
            {
                case FVar(t, null):
                {
                    var fieldType: TypePath = getFieldType(field);
                    var ct: ComplexType = TPath(fieldType);
                    var actualSize: Int = getTypeSize(ct);
                    var metadataLength: Int = getMetadataLength(field);

                    // Change var to property.
                    field.kind = FProp("get", "set", ct, null);
                    field.access.push(APublic);

                    // Generate getter and setter body.
                    var getterBody: Expr;
                    var setterBody: Expr;

                    switch fieldType.name
                    {
                        case "Float":
                            getterBody = macro { return this.getFloat($v{dataPointer}); };
                            setterBody = macro { this.setFloat($v{dataPointer}, value); return value; };
                        case "Int":
                            getterBody = macro { return this.getInt32($v{dataPointer}); };
                            setterBody = macro { this.setInt32($v{dataPointer}, value); return value; };
                        case "Int64":
                            getterBody = macro { return this.getInt64($v{dataPointer}); };
                            setterBody = macro { this.setInt64($v{dataPointer}, value); return value; };
                        case "UInt":
                            getterBody = macro { return this.getUInt16($v{dataPointer}); };
                            setterBody = macro { this.setUInt16($v{dataPointer}, value); return value; };

                        case "String" if (metadataLength < 0):
                            Context.error('The String type requires a length(n) metadata attribute.', field.pos);

                        case "String":
                            getterBody = macro { return StringTools.replace(this.getString($v{dataPointer}, $v{metadataLength}), "\u0000", ""); };
                            setterBody = macro { this.blit($v{dataPointer}, Bytes.ofString(value), 0, cast Math.min($v{metadataLength}, Bytes.ofString(value).length)); return value; };
                            actualSize += metadataLength * 1;

                        default:
                            Context.error('Type not supported for net messages: ${fieldType.name}', field.pos);
                    }
                    dataPointer += actualSize;

                    // Create the getter.
                    fields.push({
                        name: "get_" + field.name,
                        pos: Context.currentPos(),
                        kind: FFun({
                            ret: ct,
                            args: [],
                            expr: getterBody
                        }),
                        access: [ AInline ]
                    });

                    // Create the setter.
                    fields.push({
                        name: "set_" + field.name,
                        pos: Context.currentPos(),
                        kind: FFun({
                            ret: ct,
                            args: [{
                                name: "value",
                                type: ct
                            }],
                            expr: setterBody
                        }),
                        access: [ AInline ]
                    });

                    // Add field to the constructor args.
                    ctorArgs.set({
                        name: field.name,
                        type: ct
                    },
                    actualSize);
                }

                default:
            }
        }


        /*
            Compute total message size.
        */
        var msgSize: Int = dataPointerStart;
        for (arg => size in ctorArgs)
        {
            msgSize += size;
        }


        /*
            Create constructor body.
        */
        var ctorBody: StringBuf = new StringBuf();
        ctorBody.add('this = Bytes.alloc($msgSize);\n');
        ctorBody.add('this.setInt32(0, $code);\n');
        dataPointer = dataPointerStart;
        for (arg => size in ctorArgs)
        {
            var line: String = switch getTypeName(arg.type)
            {
                case "Float": 'this.setFloat($dataPointer, ${arg.name});';
                case "Int": 'this.setInt32($dataPointer, ${arg.name});';
                case "Int64": 'this.setInt64($dataPointer, ${arg.name});';
                case "UInt": 'this.setUInt16($dataPointer, ${arg.name});';
                case "String": 'this.blit($dataPointer, Bytes.ofString(${arg.name}), 0, cast Math.min($size, Bytes.ofString(${arg.name}).length));';

                default: "";
            }
            dataPointer += size;

            ctorBody.add(line);
            ctorBody.add('\n');
        }


        /*
            Create constructor.
        */
        var ctorArgsArray: Array<FunctionArg> = [];
        for (arg in ctorArgs.keys())
        {
            ctorArgsArray.push(arg);
        }
        fields.push({
            name: "new",
            pos: Context.currentPos(),
            kind: FFun({
                args: ctorArgsArray,
                expr: Context.parse('{ ' + ctorBody.toString() + ' }', Context.currentPos()),
                ret: null
            }),
            access: [ APublic, AInline ]
        });


        /*
            Create reset method identical to constructor, but with return this at the end.
        */
        var abstractPack: Array<String> = Context.getLocalClass().get().pack.slice(0, -1);
        var abstractName: String = Context.getLocalClass().get().name.replace('_Impl_', '');
        ctorBody.add('return cast this;');
        fields.push({
            name: "reset",
            pos: Context.currentPos(),
            kind: FFun({
                args: ctorArgsArray,
                expr: Context.parse('{ ' + ctorBody.toString() + ' }', Context.currentPos()),
                ret: TPath({ pack: abstractPack, name: abstractName })
            }),
            access: [ APublic, AInline ],
            doc: '
                Re-initializes the object with new field values.

                The parameters of this method will always be the same as the constructor\'s.

                Returns the object itself, for chaining.
            '
        });


        /*
            Add byte size field.
        */
        fields.push({
            name: 'size',
            pos: Context.currentPos(),
            access: [ APublic, AStatic, AInline ],
            kind: FVar(macro: Int, macro $v{msgSize}),
            doc: 'The length of this type in bytes.'
        });


        return fields;

        #else
        return [];
        #end
    }


    static function getTypeSize(type: ComplexType): Int
    {
        return switch getTypeName(type)
        {
            case "Float": 4;
            case "Int": 4;
            case "Int64": 8;
            case "UInt": 2;
            case "String": 0; // Size will be determined from attribute.

            default: -1;
        }
    }


    static function getMetadataLength(field: Field): Int
    {
        for (meta in field.meta)
        {
            if (meta.name == 'length' && meta.params.length == 1)
            {
                return cast meta.params[0].getValue();
            }
        }
        return -1;
    }


    static function getTypeName(type: ComplexType): String
    {
        switch type
        {
            case TPath(p):
            {
                return p.name;
            }

            default:
        }
        Context.error("Error: getTypeSize()", Context.currentPos());
        return "";
    }


    static function getFieldType(field: Field): TypePath
    {
        switch (field.kind)
        {
            case FVar(t, null):
            {
                switch t
                {
                    case TPath(p):
                    {
                        return p;
                    }
                    default:
                }
            }
            default:
        }
        Context.error("Error: getFieldType()", field.pos);
        return null;
    }
}
#end
