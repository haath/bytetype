package tests;

import seedyrng.Random;
import utest.Assert;
import bytetype.ByteType;
import utest.ITest;
import haxe.io.Bytes;
import seedyrng.Seedy;
import models.*;


class TestByteType implements ITest
{
    var calledInts: Bool;
    var calledMixed: Bool;
    static var calledStrings: Bool;
    var calledStringsWrongLength: Bool;


    public function new() { }


    function setup()
    {
        calledInts = false;
        calledMixed = false;
        calledStrings = false;
        calledStringsWrongLength = false;
    }


    function testHash()
    {
        Seedy.instance.setStringSeed("bytetype");

        /**
            Check for conflicts.
        **/
        var hashes: Map<Int, Bool> = [];
        for (i in 0...10000)
        {
            var s: String = getRandomString();
            var h: Int = ByteType.hash(s);

            if (hashes.exists(h))
            {
                Assert.fail('$s -> $h');
                return;
            }
            hashes.set(h, true);
        }
        Assert.pass();


        /**
            Check invalid characters.
        **/
        Assert.equals(-1, ByteType.hash('-'));
        Assert.equals(-1, ByteType.hash('<'));
        Assert.equals(-1, ByteType.hash(']'));
        Assert.equals(-1, ByteType.hash('{'));
    }


    function testProcess()
    {
        ByteType.process(new TestTypeInts(1, 2, 3), "models");
        Assert.isTrue(calledInts);
        Assert.isFalse(calledMixed);
        Assert.isFalse(calledStrings);
        Assert.isFalse(calledStringsWrongLength);

        ByteType.process(new TestTypeStrings("one", "two", "three"), "models");
        Assert.isTrue(calledInts);
        Assert.isFalse(calledMixed);
        Assert.isTrue(calledStrings);
        Assert.isFalse(calledStringsWrongLength);

        ByteType.process(new TestTypeMixed(1.5, 2, 3, "asdf", 4), "models");
        Assert.isTrue(calledInts);
        Assert.isTrue(calledMixed);
        Assert.isTrue(calledStrings);
        Assert.isFalse(calledStringsWrongLength);

        ByteType.process(new TestTypeStringWrongLength("asdf"), "models");
        Assert.isTrue(calledInts);
        Assert.isTrue(calledMixed);
        Assert.isTrue(calledStrings);
        Assert.isFalse(calledStringsWrongLength); // this callback has the wrong name
    }


    function testProcessAll()
    {
        var buf: Bytes = Bytes.alloc(TestTypeInts.size + TestTypeStrings.size);
        buf.blit(0, new TestTypeInts(1, 2, 3), 0, TestTypeInts.size);
        buf.blit(TestTypeInts.size, new TestTypeStrings("one", "two", "three"), 0, TestTypeStrings.size);

        ByteType.processAll(buf, "models");
        Assert.isTrue(calledInts);
        Assert.isFalse(calledMixed);
        Assert.isTrue(calledStrings);
        Assert.isFalse(calledStringsWrongLength);
    }


    function callbackTestTypeInts(tt: TestTypeInts)
    {
        calledInts = true;

        Assert.equals(1, tt.one);
        Assert.equals(2, tt.two);
        Assert.equals(3, tt.three);

    }
    function callbackTestTypeMixed(tt: TestTypeMixed)
    {
        calledMixed = true;
    }
    function callbackTestTypeStrings(tt: TestTypeStrings)
    {
        calledStrings = true;
    }
    function callbackTestTypeStringWrongLengthMISTAKE(tt: TestTypeStringWrongLength)
    {
        calledStringsWrongLength = true;
    }


    function getRandomString(): String
    {
        var b: Bytes = Bytes.alloc(12);

        for (i in 0...12)
        {
            b.set(i, Seedy.instance.randomInt(97, 122));
        }

        return b.toString();
    }
}
