package models;

import haxe.io.Bytes;


@:build(bytetype.ByteTypeBuilder.build())
abstract TestTypeStrings(Bytes) to Bytes
{
    @length(4) var string1: String;
    @length(4) var string2: String;
    @length(4) var string3: String;
}
