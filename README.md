<div align="center">

[![ ](https://gitlab.com/haath/bytetype/-/raw/master/assets/logo.png)](https://gitlab.com/haath/bytetype)

[![pipeline status](https://gitlab.com/haath/bytetype/badges/master/pipeline.svg)](https://gitlab.com/haath/bytetype/pipelines/latest)
[![coverage report](https://gitlab.com/haath/bytetype/badges/master/coverage.svg)](https://gitlab.com/haath/bytetype/pipelines/latest)
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://gitlab.com/haath/bytetype/blob/master/LICENSE)
[![release](https://img.shields.io/badge/release-haxelib-informational)](https://lib.haxe.org/p/bytetype/)

</div>

---

Library for defining datatypes over byte array abstracts, primarily to be used for network messages.
Utilizing macros and Haxe abstracts, this allows for pseudo-classes to be defined, which can have fields of various
types and even their own methods as any normal abstract would. However during runtime these objects will only be byte arrays,
which makes them efficient for network communication, since they don't need any additional overhead for serialization.

Currently supported field types:

- `Int`
- `Float`
- `Int64`
- `UInt` (16-bit)
- `String` (fixed-length)


## Installation

```bash
haxelib install bytetype
```


## Usage

Describe typed objects, as abstracts over byte arrays like in the following example.

```haxe
@:build(bytetype.ByteTypeBuilder.build())
abstract MyAbstractType(Bytes)
{
    var one: Int;
    var two: Int;
    var three: Float;
}
```

The build macro will then generate a constructor, as well as getters for each field.
The constructor will have one parameter for each field, in the order in which they appear in the abstract.
So the object can be used like this:

```haxe
var x = new MyAbstractType(1, 2, 3);

trace(x.one); // 1
trace(x.two); // 2

x.three = 666;
trace(x.three); // 666
```


### Receiving messages

Each type the builder is used on, will have a unique integer code assigned to it, which is accessible on the static property `code`.

When receiving messages, the `getCode()` function can be used to check which type of message was received.

```haxe
var message: Bytes;

switch ByteType.getCode(message)
{
    case Hello.code:
        var helloMsg: Hello = cast message;
        trace( helloMsg.username );

    case Goodbye.code:
        var goodbyeMsg: Goodbye = cast message;
}
```


### Generating callbacks

Instead of comparing the codes manually, the library also comes with a callback dispatch generator.
In the following example, suppose that we have the same two types `Hello` and `Goodbye` as above, and that they are both under the `my.messages` package path.

The `process()` macro will generate code, which compares the message code to all types listed under `my.messages`.
It then passes the message to the respective callback function with the correct naming convention.

```haxe
function onMessageReceived(message: Bytes)
{
    ByteType.process(message, "my.messages");
}

function callbackHello(msg: Hello) { }

function callbackGoodbye(msg: Goodbye) { }
```

**Note:**
- The naming conventions need to be exact. The callback for type `Foo` needs to be named `callbackFoo` and have one argument of the desired type.
- The package path given to `process()` needs to **only** contain types as described here, abstract types over `Bytes` annotated with `ByteTypeBuilder`.
- Process callbacks by default are optional, and if any don't exist in the scope of the call to `process()` they will simply not have their calls generated to avoid compilation errors. This behavior can be changed by passing `false` to the `callbacksOptional` parameter.


### Multiple messages in a single buffer

Often when communicating with sockets, multiple smaller packets will get combined into larger ones to optimize traffic.
For this reason the library also contains a `processAll()` macro, which will correctly identify multiple bytetype objects that have been packed together into a single byte buffer, and invoke the callback of each one like above.

In this example a `Hello` and a `Goodbye` message are both packed into a single `Bytes` buffer.
Using `processAll()` both of their callbacks will be invoked.

```haxe
function foo()
{
    var buffer: Bytes = Bytes.alloc(Hello.size + Goodbye.size);
    buf.blit(0,          new Hello("username"), 0, Hello.size);      // Hello message is written in the first Hello.size bytes
    buf.blit(Hello.size, new Goodbye(),         0, Goodbye.size);   // Goodbye message is written in the next bytes.

    ByteType.processAll(message, "my.messages");
}

function callbackHello(msg: Hello) { }

function callbackGoodbye(msg: Goodbye) { }
```

Note also the existence of the static `size` property, which is generated to denote the total size in bytes of the specific message type.


### Strings

The string type works just like the rest, but it also requires a fixed length to be specified with metadata.
The length specifies the maximum number of characters, and it's also the size that the string will **always** occupy in the byte array,
so setting it to a very large value might not be preferable.

```haxe
@:build(bytetype.ByteTypeBuilder.build())
abstract LoginMessage(Bytes)
{
    @length(10) var username: String;
}
```

Passing a string longer than the specified length to the constructor will have undefined results.


### Reusing objects

To reuse an object, the generated `reset()` may be used, which will be identical to the constructor.

```haxe
var loginMsg = new LoginMessage("my_username");

mySocket.send(loginMsg);

loginMsg.reset("my_other_username");

mySocket.send(loginMsg);

// The init method also returns the object itself.
mySocket.send( loginMsg.reset("yet_another") );
```

